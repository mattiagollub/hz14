package com.hackzurich.fridgy;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mattia on 11.10.14.
 */
public class Recipe {

    public String attributionLogo;

    public String attributionText;

    public String attributionURL;

    public List<String> ingredients;

    public String name;

    public String sourceSite;

    public Recipe(String recipeID) {
        String request = "http://api.yummly.com/v1/api/recipe/" + recipeID + "?_app_id=" +
                Config.APP_ID +  "&_app_key=" + Config.APP_KEY;

        try {
            String result;
            while ((result = new HttpQuery().execute(request).get()) == null);

            JSONObject response = new JSONObject(result);

            JSONObject attrObject = response.getJSONObject("attribution");
            attributionLogo = attrObject.getString("logo");
            attributionText = attrObject.getString("text");
            attributionURL = attrObject.getString("url");

            JSONArray ingredientsObj = response.getJSONArray("ingredientLines");
            ingredients = new ArrayList<String>();

            for (int i = 0; i < ingredientsObj.length(); i++) {
                ingredients.add(ingredientsObj.getString(i));
            }

            name = response.getString("name");
            sourceSite = response.getJSONObject("source").getString("sourceRecipeUrl");
        }
        catch (Exception e)
        {
            name = e.getMessage();
        }
    }
}
