package com.hackzurich.fridgy;

import java.math.BigInteger;
import java.util.List;

public abstract class Store
{
    public abstract List<String> searchProduct(String name);

    public abstract BigInteger getEAN(String name);

    public abstract boolean isDiscounted(BigInteger ean);

    public abstract ProductInfo getProductInfo(BigInteger ean);

    public abstract NutritionFacts getNutritionFacts(BigInteger ean);
}
