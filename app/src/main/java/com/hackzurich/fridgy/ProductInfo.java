package com.hackzurich.fridgy;

public class ProductInfo
{
    Integer price;
    Double quantity;
    String unit;

    ProductInfo(Integer price, Double quantity, String unit)
    {
        this.price = price;
        this.quantity = quantity;
        this.unit = unit;
    }
}