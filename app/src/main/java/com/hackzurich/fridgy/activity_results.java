package com.hackzurich.fridgy;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;


public class activity_results extends Activity {

    public final static String EXTRA_MESSAGE = "com.hackzurich.fridgy.MESSAGE";

    private List<SearchResult> searchResults;
    private List<SearchResult> moreResults = null;

    private List<String> results;
    private ArrayAdapter<String> arrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity_results);

        Intent intent = getIntent();
        String query = intent.getStringExtra(Search.EXTRA_MESSAGE);

        RecipeDB recDb = new RecipeDB();
        searchResults = recDb.SearchRecipes(query);

        if (searchResults.size() > 3)
        {
            moreResults = searchResults.subList(3, searchResults.size());
            searchResults = searchResults.subList(0, 3);
        }

        results = new ArrayList<String>();
        for (SearchResult sr : searchResults)
            results.add(sr.title);

        ListView list = (ListView)findViewById(R.id.listView);

        arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, results);
        list.setAdapter(arrayAdapter);

        if (moreResults == null)
        {
            Button btn = (Button)findViewById(R.id.button);
            btn.setVisibility(View.GONE);
        }

        list.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView<?> arg0, View v, int position, long arg3)
            {
                Intent intent = new Intent(activity_results.this, Recipe_screen.class);

                intent.putExtra(EXTRA_MESSAGE, searchResults.get(position).id);
                startActivity(intent);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_results, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onShowMoreClicked(View view) {
//        searchResults.addAll(moreResults); // Fuck this.

        //final List<String> results = new ArrayList<String>();
        for (SearchResult sr : moreResults)
            results.add(sr.title);

        arrayAdapter.notifyDataSetChanged();

        Button btn = (Button)findViewById(R.id.button);
        btn.setVisibility(View.GONE);
    }
}
