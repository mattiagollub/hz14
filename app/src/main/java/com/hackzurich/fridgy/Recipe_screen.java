package com.hackzurich.fridgy;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public class Recipe_screen extends Activity {

    protected Recipe rec;
    protected List<String> ingredients;
    protected List<String> listItems;
    protected ArrayAdapter<String> arrayAdapter;
    protected Migros migros;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_screen);

        Intent intent = getIntent();
        String rec_id = intent.getStringExtra(activity_results.EXTRA_MESSAGE);
        rec = new Recipe(rec_id);

        TextView textView =(TextView)findViewById(R.id.textView);
        textView.setText(rec.name);

        ListView listView = (ListView)findViewById(R.id.listView);

        /* Ups... Soon will be fixed;) */
        ingredients = new ArrayList<String>();
        ingredients.add("M-Budget Tomaten");
        ingredients.add("M-Budget Hackfleisch gemischt");
        ingredients.add("M-Budget Mozzarella");
        ingredients.add("M-Classic Schweizer Eier Bodenhaltung");

        Integer tot = 0;
        migros = new Migros();
        listItems = new ArrayList<String>();
        for (String s : ingredients)
        {
            ProductInfo pi = migros.getProductInfo(migros.getEAN(s));
            listItems.add(s + " (" + (pi.price / 100D) + " CHF" +
                    (!pi.quantity.equals(0D)
                    ? (", " + pi.quantity + " " + pi.unit + ")")
                    : ")"));
            tot += pi.price;
        }

        arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listItems);
        listView.setAdapter(arrayAdapter);

        TextView totTextView = (TextView)findViewById(R.id.totTextView);
        totTextView.setText("Tot: " + (tot / 100D) + " CHF");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.recipe_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onShopClick(View view) {
        Intent intent = new Intent(this, Shop_position.class);
        startActivity(intent);
    }

    public void onDirectionsClick(View view) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(rec.sourceSite));
        startActivity(browserIntent);
    }
}
