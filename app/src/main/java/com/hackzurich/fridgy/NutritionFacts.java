package com.hackzurich.fridgy;

import java.util.List;

public class NutritionFacts
{
    Integer baseQuantity;
    Integer baseUnit;
    List<Nutrient> nutrients;

    NutritionFacts(Integer baseQuantity, Integer baseUnit, List<Nutrient> nutrients)
    {
        this.baseQuantity = baseQuantity;
        this.baseUnit = baseUnit;
        this.nutrients = nutrients;
    }
}