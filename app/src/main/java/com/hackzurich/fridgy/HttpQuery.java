package com.hackzurich.fridgy;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by mattia on 11.10.14.
 */
public class HttpQuery extends AsyncTask<String, Void, String>
{
    protected String doInBackground(String... urls)
    {
        try
        {
            StringBuilder sb = new StringBuilder();
            URL url = new URL(urls[0]);
            URLConnection connection = url.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            while ((inputLine = in.readLine()) != null)
                sb.append(inputLine).append("\n");
            in.close();
            return sb.toString();
        }
        catch (Exception ex)
        {
            return null;
        }
    }
}
