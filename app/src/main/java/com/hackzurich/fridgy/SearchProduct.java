package com.hackzurich.fridgy;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.math.BigInteger;
import java.util.List;


public class SearchProduct extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_product);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.search_product, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onButtonClick(View view)
    {
        try
        {
            ListView list = (ListView)findViewById(R.id.listView);
            EditText text = (EditText)findViewById(R.id.txtRecipe);

            final Migros migros = new Migros();
            final List<String> results = migros.searchProduct(text.getText().toString());

            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, results);
            list.setAdapter(arrayAdapter);

            list.setOnItemClickListener(new AdapterView.OnItemClickListener()
            {
                public void onItemClick(AdapterView<?> arg0, View v, int position, long arg3)
                {
                    BigInteger id = migros.getEAN(results.get(position));
                    if (id == null)
                        id = new BigInteger("-1");
                    Toast.makeText(getApplicationContext(), "ID: " + id.toString(), Toast.LENGTH_LONG).show();
                }
            });
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
    }
}
