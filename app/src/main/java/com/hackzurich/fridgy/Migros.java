package com.hackzurich.fridgy;

import android.os.AsyncTask;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public class Migros extends Store
{
    public FakeMigrosAPI fakeMigrosAPI = new FakeMigrosAPI();
    protected String baseUrl = "http://api.autoidlabs.ch/";

    @Override
    public List<String> searchProduct(String name)
    {
        try
        {
            String result;
            while ((result = new HttpQuery().execute(baseUrl + "/search?text=" + name).get()) == null);

            JSONObject obj = new JSONObject(result);
            JSONArray arr = obj.getJSONArray("suggestions");

            List<String> suggestions = new ArrayList<String>(arr.length());
            for (int i = 0; i < arr.length(); i++)
                suggestions.add(arr.getJSONObject(i).getString("name"));

            return suggestions;
        }
        catch (Exception e)
        {
            return null;
        }
    }

    @Override
    public BigInteger getEAN(String name)
    {
        return fakeMigrosAPI.getProductEAN(name);
    }

    @Override
    public boolean isDiscounted(BigInteger ean)
    {
        try
        {
            String result;
            while ((result = new HttpQuery().execute(baseUrl + "/products/" + ean.toString()).get()) == null);

            JSONObject obj = new JSONObject(result);
            return obj.getBoolean("discounted");
        }
        catch (Exception e)
        {
            return false;
        }
    }

    @Override
    public ProductInfo getProductInfo(BigInteger ean)
    {
        try
        {
            String url = "https://test-web-api.migros.ch/eth-hack/products/" + fakeMigrosAPI.getSAPfromEAN(ean) + "?key=k0DFQajkP8AnGMF9";

            String result;
            while ((result = new HttpQuery().execute(url).get()) == null);

            JSONObject obj = new JSONObject(result)
                    .getJSONObject("regional_information")
                    .getJSONObject("gmaa")
                    .getJSONObject("price")
                    .getJSONObject("item");

            Integer price = (int)(obj.getDouble("price") * 100);
            Double quantity;
            String unit;
            try
            {
                quantity = obj.getDouble("quantity");
                unit = obj.getString("unit");
            }
            catch (Exception e)
            {
                quantity = 0D;
                unit = "";
            }

            return new ProductInfo(price, quantity, unit);
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
            return new ProductInfo(0, 0D, e.getMessage());
        }
    }

    @Override
    public NutritionFacts getNutritionFacts(BigInteger ean)
    {
        return null;
    }
}
