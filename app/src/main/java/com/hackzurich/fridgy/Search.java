package com.hackzurich.fridgy;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;


public class Search extends Activity {

    public final static String EXTRA_MESSAGE = "com.hackzurich.fridgy.MESSAGE_SEARCH";
    public final static String EXTRA_MESSAGE_MAP = "com.hackzurich.fridgy.MESSAGE_MAP";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void goToSearchProduct(View view) {
        Intent intent = new Intent(this, SearchProduct.class);
        startActivity(intent);
    }

    public void onPreferencesClick(View view) {
        Intent intent = new Intent(this, Preferences.class);
        startActivity(intent);
    }

    public void onclicksearch(View view) {
        RecipeDB db = new RecipeDB();

        try {
            TextView txtRecipe = (TextView) findViewById(R.id.txtRecipe);
            TextView txtIngredients = (TextView) findViewById(R.id.txtIngredients);

            String rec = (txtRecipe.getText().toString()).replace(' ', '+');
            String ing = (txtIngredients.getText().toString()).replace(" ", "&allowedIngredient[]=");

            if (!rec.equals(""))
                rec = "&q=" + rec;
            if (!ing.contentEquals(""))
                ing = "&allowedIngredient[]=" + ing;


            String query = "http://api.yummly.com/v1/api/recipes?_app_id=" + Config.APP_ID + "&_app_key=" + Config.APP_KEY + rec + ing;

            Intent intent = new Intent(Search.this, activity_results.class);

            intent.putExtra(EXTRA_MESSAGE, query);
            startActivity(intent);

        } catch (Exception e) {
          //  return false;
        }
    }
}
