package com.hackzurich.fridgy;

public class Nutrient
{
    Integer quantity;
    Integer unit;

    Nutrient(Integer quantity, Integer unit)
    {
        this.quantity = quantity;
        this.unit = unit;
    }
}